
Multidimensional Polynomials
============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorials
   howto
   discussions
   reference

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
