``mdpoly`` or Multidimensional Polynomials
==========================================

*Work in progress!*

Quick Start
-----------

There is still a big part of the API missing but the snipped below works enough
to give an overview.

.. code:: py

        from mdpoly import State, Variable, Parameter
        from mdpoly.representations import SparseRepr

        # Construct an expression
        x, y = Variable.from_names("x, y") # or just Variable("x")
        k = Parameter("k")

        p = (x + 2 * y) ** 3 + y ** 2 + k
        print(f"{p = }")

        # Expressions can be easily reparametrized
        w = Parameter("w")
        q = p.replace(y, w).replace(k, k ** 2) # createas a copy
        print(f"{q = }")

        # Make a concrete representation
        state = State(parameters={k: 3.14}) # try to replace with empty dict
        sparse, state = p.to_repr(SparseRepr, state)

        # Look inside the representation
        for entry in sparse.entries():
            print(f"at (row, col) = {entry.row, entry.col} there is a polynomial:")
            for term in sparse.terms(entry):
                monomial_str = ""
                for idx in term:
                    var = state.from_index(idx)
                    monomial_str += f"{var}^{idx.power} "

                # Get the coefficient
                coeff = sparse.at(entry, term)
                print(" - the monomial", monomial_str, "has coefficient", coeff)

        # You can also simply iterate over it
        for entry, term, coeff in sparse:
            print(entry, term, coeff)

There is some advanced stuff that is still broken but the idea is that it will
work soon-ish.

.. code:: py

        from mdpoly import Variable, MatrixVariable
        from mdpoly.types import Shape

        x = Variable("x")
        V = MatrixVariable("V", Shape.column(3))

        print(x.shape, V.shape)

        z = x + V # error
        scalar = V.T @ V # no error (TODO)
