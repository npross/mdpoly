try:
    import mdpoly
except ModuleNotFoundError:
    import sys
    import pathlib
    parent = pathlib.Path(__file__).resolve().parent.parent
    sys.path.append(str(parent))

from mdpoly import State, Variable, Parameter
from mdpoly.representations import SparseRepr

def make_invalid_expr(x, y):
    return x ** (-4)

def make_another_invalid(x, y):
    return x ** -1

def make_ok_expr(x, y):
    w = make_invalid_expr(x, y)
    z = make_another_invalid(x, y)
    return x + z + y + w

x, y, z = Variable.from_names("x, y, z")

bad = make_ok_expr(x, y)
bad.to_sparse(State())
