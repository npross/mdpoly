from __future__ import annotations
from typing import TYPE_CHECKING

from typing import Iterable
import cvxopt

from ..abc import Repr

if TYPE_CHECKING:
    from ..index import MatrixIndex, PolyIndex, Number, Shape


class CVXOptSparseMatrixRepr(Repr):
    data: cvxopt.spmatrix

    def __init__(self, shape: Shape):
        self.data = cvxopt.spmatrix(0, size=shape)

    def at(self, entry: MatrixIndex, term: PolyIndex) -> Number:
        """ Access polynomial coefficient. """
        return self.data[entry.row, entry.col]

    def set(self, entry: MatrixIndex, term: PolyIndex, value: Number) -> None:
        """ Set value of polynomial coefficient. """
        raise NotImplementedError

    def set_zero(self, entry: MatrixIndex, term: PolyIndex) -> None:
        """ Set a coefficient to zero (delete it). """
        raise NotImplementedError

    def entries(self) -> Iterable[MatrixIndex]:
        """ Return indices to non-zero entries of the matrix. """
        raise NotImplementedError

    def terms(self, entry: MatrixIndex) -> Iterable[PolyIndex]:
        """ Return indices to non-zero terms in the polynomial at the given
        matrix entry. """
        raise NotImplementedError

