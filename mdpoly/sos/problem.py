from __future__ import annotations
from typing import TYPE_CHECKING

from typing import List
from dataclasses import dataclass

from ..abc import Var, Rel
from ..expressions import Expression

if TYPE_CHECKING:
    pass


class OptVar(Var):
    """ Leaf to represent an optimization variable. """


@dataclass(frozen=True)
class Problem:
    """ Optimization problem. """
    objective: Expression
    variables: List[OptVar]
    constraints: List[Rel]


@dataclass(frozen=True)
class SOSProblem(Problem):
    ...


@dataclass(frozen=True)
class SOSCertProblem(Problem):
    ...
