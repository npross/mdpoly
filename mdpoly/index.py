from __future__ import annotations
from typing import TYPE_CHECKING

from typing import Self, NamedTuple, Optional, Sequence
from itertools import filterfalse

from .errors import InvalidShape
from .util import partition, isclose

if TYPE_CHECKING:
    from .state import State
    from .abc import Var

# FIXME: this is not really an appropriate place for this -> constants?
Number = int | float


class Shape(NamedTuple):
    """ Describes the shape of a mathematical object. """
    rows: int
    cols: int

    @property
    def infer(self) -> Self:
        return self.__class__(-1, -1)

    @classmethod
    def scalar(cls) -> Self:
        return cls(1, 1)
    
    @classmethod
    def row(cls, n: int) -> Self:
        if n <= 0:
            raise InvalidShape("Row vector must have dimension >= 1")
        return cls(1, n)

    def is_row(self) -> bool:
        return self.cols == 1

    @classmethod
    def col(cls, n: int) -> Self:
        if n <= 0:
            raise InvalidShape("Column vector must have dimension >= 1")
        return cls(n, 1)

    def is_col(self) -> bool:
        return self.rows == 1

    # --- aliases / shorthands ---

    @property
    def columns(self):
        return self.cols

    @classmethod
    def column(cls, n: int) -> Self:
        return cls.col(n)

    def is_column(self) -> bool:
        return self.is_col()


class MatrixIndex(NamedTuple):
    """ Tuple to index an element of a matrix or vector. """
    row: int
    col: int

    @classmethod
    def infer(cls, row=-1, col=-1) -> Self:
        return cls(row, col)

    @classmethod
    def scalar(cls):
        """ Shorthand for index of a scalar """
        return cls(row=0, col=0)


class PolyVarIndex(NamedTuple):
    """ Tuple to represent a variable and its exponent. They are totally
    ordered with respect to the variable index stored in the ``state`` object
    (see :py:attr:`mdpoly.state.State.variables`).

    Concretely this represents things like :math:`x^2`, :math:`y^4` but not
    :math:`xy`.
    """
    var_idx: int # Index in State.variables, not variable!
    power: Number
    
    # --- Magic methods ---

    def __eq__(self, other):
        if type(other) is not PolyVarIndex:
            other = PolyVarIndex(other)

        if self.var_idx != other.var_idx:
            return False

        if not isclose(self.power, other.power):
            return False
        
        return True

    def __lt__(self, other):
        if type(other) is not PolyVarIndex:
            other = PolyVarIndex(other)

        return self.var_idx < other.var_idx

    # -- Helper methods

    @property
    def exponent(self):
        """ Alias for ``self.power``. """
        return self.power

    def is_constant(self) -> bool:
        """ Check if is index of a constant term. """
        return self.var_idx == -1
    
    # -- Helper methods for construction ---

    @classmethod
    def from_var(cls, variable: Var, state: State, power: Number =1) -> Self:
        """ Make an index from a variable object. """
        return cls(var_idx=state.index(variable), power=power)

    @classmethod
    def constant(cls) -> Self:
        """ Special index for constants.

        Constants do not have an associated variable, and the field power is
        ignored.
        """
        return cls(var_idx=-1, power=0)


class PolyIndex(tuple[PolyVarIndex]):
    """
    Tuple to index a coefficient of a monomial in a polynomial.

    For example, suppose there are two variables :math:`x, y` with indices 0, 1
    respectively (in the State object, see :py:attr:`mdpoly.state.State.variables`).
    Then the monomal :math:`xy^2` has an index

    .. code:: py

       PolyIndex(PolyVarIndex(var_idx=0, power=1), PolyVarIndex(var_idx=1, power=2)

    Then given the polynomial

    .. math::
       p(x, y) = 3x^2 + 5xy^2

    The with the ``PolyIndex`` above we can retrieve the coefficient 5 in front
    of :math:`xy^2` from the ``Repr`` object (see also :py:meth:`mdpoly.abc.Repr.at`).
    """

    # -- Magic methods ---

    def __repr__(self) -> str:
        name = self.__class__.__qualname__
        indices = ", ".join(map(repr, self))
        return f"{name}({indices})"

    # FIXME: may need to convert to method to avoid breaking expected behaviour
    def __contains__(self, index: PolyVarIndex | int) -> bool:
        if isinstance(index, PolyVarIndex):
            return tuple.__contains__(self, index)

        if isinstance(index, int):
            return any(map(lambda e: e.var_idx == index, self))

    # -- Helper methods ---

    def as_multi_index(self) -> Sequence[Number]:
        """ Get the multi-index of the monomial term.

        For example for an index representing :math:`x^2 y` the multi-index is
        a tuple :math:`(2, 1)`. Note that :math:`yx^2` has also the same
        multi-index :math:`(2, 1)`. The order in which the exponents (powers)
        are given in the tuple is ascending acording to the index of the
        variables in the state (see total order of
        :py:class:`mdpoly.index.PolyVarIndex`). In the given example then
        :math:`x` would have index 0 and :math:`y` index 1.

        The multi-index can be used to compute the Newton polytope of a
        polynomial (which is the convex hull of the multi-indices).
        """
        def take_power(idx: PolyVarIndex) -> Number:
            return idx.power

        return tuple(map(take_power, sorted(self)))

    def is_constant(self) -> bool:
        """ Check if is index of a constant term. """
        if len(self) != 1:
            return False

        return PolyVarIndex.is_constant(self[0])

    # --- Helper methods for construction ---

    @classmethod
    def from_dict(cls, d) -> Self:
        """ Construct an index from a dictionary, where the keys are the
        variable indices from the state object and the values are the exponents
        (powers). """
        return cls(PolyVarIndex(k, v) for k, v in d.items())

    @classmethod
    def from_expr(cls, product_of_vars) -> Self:
        # TODO: implement product of variables
        raise NotImplementedError

    @classmethod
    def constant(cls) -> Self:
        """ Index of the constant term. """
        return cls((PolyVarIndex.constant(),))

    @classmethod
    def sort(cls, index: tuple | Self) -> Self:
        """ Sort a tuple of indices. """
        return cls(sorted(index))

    @classmethod
    def product(cls, left: Self, right: Self) -> Self:
        """ Compute index of product.

        For example if left is the index of :math:`xy` and right is the index
        of :math:`y^2` this functions returns the index of :math:`xy^3`.
        """
        if left.is_constant():
            return right

        if right.is_constant():
            return left

        result: dict[int, Number] = dict(filterfalse(PolyVarIndex.is_constant, left))
        for idx, power in filterfalse(PolyVarIndex.is_constant, right):
            if idx not in result:
                result[idx] = power
            else:
                result[idx] += power

        return cls.sort(cls.from_dict(result))

    @classmethod
    def differentiate(cls, index: Self, wrt: int) -> Optional[tuple[Self, Number]]:
        """ Compute the index after differentiation

        For example if the index is :math:`xy^2` and ``wrt`` is the index of
        :math:`y` this function returns the index of :math:`xy`. The function
        also returns the exponent before it was differentiated, hence in the
        example 2.

        This function takes care of the edge cases of differentiating a
        constant and differentiating linear terms. Specifically, if the index
        is a constant ``None`` is returned.
        """

        if index.is_constant():
            return None

        if wrt not in index:
            return None

        def is_wrt_var(idx: PolyVarIndex) -> bool:
            return idx.var_idx == wrt

        others, with_wrt_var = partition(is_wrt_var, index)
        with_wrt_var, *_ = with_wrt_var # Take first, as should be only one

        # Check if is linear term
        if isclose(with_wrt_var.power, 1.):
            return cls.sort(others), 1

        # Decrease exponent
        new_idx = PolyVarIndex(var_idx=wrt, power=(with_wrt_var.power - 1))
        return cls.sort(tuple(others) + (new_idx,)), with_wrt_var.power
