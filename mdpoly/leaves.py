from __future__ import annotations
from typing import TYPE_CHECKING

from dataclassabc import dataclassabc
from typing import Type, TypeVar, Iterable, Sequence

from .abc import Var, Const, Param
from .index import Shape, MatrixIndex, PolyIndex, PolyVarIndex, Number

if TYPE_CHECKING:
    from .abc import ReprT
    from .state import State


# ┏┳┓┏━┓╺┳╸┏━┓╻┏━╸┏━╸┏━┓
# ┃┃┃┣━┫ ┃ ┣┳┛┃┃  ┣╸ ┗━┓
# ╹ ╹╹ ╹ ╹ ╹┗╸╹┗━╸┗━╸┗━┛


@dataclassabc(frozen=True)
class MatConst(Const):
    """ Matrix constant. TODO: desc. """
    value: Sequence[Sequence[Number]] # Row major, overloads Const.value
    shape: Shape # overloads Expr.shape
    name: str = "" # overloads Leaf.name

    def to_repr(self, repr_type: Type[ReprT], state: State) -> tuple[ReprT, State]:
        r = repr_type(self.shape)

        for i, row in enumerate(self.value):
            for j, val in enumerate(row):
                r.set(MatrixIndex(row=i, col=j), PolyIndex.constant(), val)

        return r, state


    def __str__(self) -> str:
        if not self.name:
            return repr(self.value)

        return self.name


T = TypeVar("T", bound=Var)

@dataclassabc(frozen=True)
class MatVar(Var):
    """ Matrix of polynomial variables. TODO: desc """
    name: str # overloads Leaf.name
    shape: Shape # overloads Expr.shape

    # TODO: review this API, can be moved elsewhere?
    def to_scalars(self, scalar_var_type: Type[T]) -> Iterable[tuple[MatrixIndex, T]]:
        for row in range(self.shape.rows):
            for col in range(self.shape.cols):
                var = scalar_var_type(name=f"{self.name}_[{row},{col}]") # type: ignore[call-arg]
                entry = MatrixIndex(row, col)

                yield entry, var

    def to_repr(self, repr_type: Type[ReprT], state: State) -> tuple[ReprT, State]:
        r = repr_type(self.shape)
        
        # FIXME: do not hardcode scalar type
        for entry, var in self.to_scalars(PolyVar):
            idx = PolyVarIndex.from_var(var, state), # important comma!
            r.set(entry, PolyIndex(idx), 1)

        return r, state

    def __str__(self) -> str:
        return self.name


@dataclassabc(frozen=True)
class MatParam(Param):
    """ Matrix parameter. TODO: desc. """
    name: str
    shape: Shape

    def to_repr(self, repr_type: Type[ReprT], state: State) -> tuple[ReprT, State]:
        p = state.parameter(self)
        return MatConst(p).to_repr(repr_type, state) # type: ignore[call-arg]

    def __str__(self) -> str:
        return self.name


# ┏━┓┏━┓╻  ╻ ╻┏┓╻┏━┓┏┳┓╻┏━┓╻  ┏━┓
# ┣━┛┃ ┃┃  ┗┳┛┃┗┫┃ ┃┃┃┃┃┣━┫┃  ┗━┓
# ╹  ┗━┛┗━╸ ╹ ╹ ╹┗━┛╹ ╹╹╹ ╹┗━╸┗━┛


@dataclassabc(frozen=True)
class PolyVar(Var):
    """ Variable TODO: desc """
    name: str # overloads Leaf.name
    shape: Shape = Shape.scalar() # ovearloads PolyExpr.shape

    def to_repr(self, repr_type: Type[ReprT], state: State) -> tuple[ReprT, State]:
        r = repr_type(self.shape)
        idx = PolyVarIndex.from_var(self, state), # important comma!
        r.set(MatrixIndex.scalar(), PolyIndex(idx), 1)
        return r, state


@dataclassabc(frozen=True)
class PolyConst(Const):
    """ Constant TODO: desc """
    value: Number # overloads Const.value
    name: str = "" # overloads Leaf.name
    shape: Shape = Shape.scalar() # ovearloads PolyExpr.shape

    def to_repr(self, repr_type: Type[ReprT], state: State) -> tuple[ReprT, State]:
        r = repr_type(self.shape)
        r.set(MatrixIndex.scalar(), PolyIndex.constant(), self.value)
        return r, state


@dataclassabc(frozen=True)
class PolyParam(Param):
    """ Polynomial parameter TODO: desc """
    name: str # overloads Leaf.name
    shape: Shape = Shape.scalar() # overloads PolyExpr.shape

    def to_repr(self, repr_type: Type[ReprT], state: State) -> tuple[ReprT, State]:
        p = state.parameter(self)
        return PolyConst(p).to_repr(repr_type, state) # type: ignore[call-arg]
