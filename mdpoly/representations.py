from __future__ import annotations
from typing import TYPE_CHECKING

from typing import Iterable
import numpy as np

from .abc import Repr 

if TYPE_CHECKING:
    import numpy.typing as npt
    from .index import Number, Shape, MatrixIndex, PolyIndex


class SparseRepr(Repr):
    """ Sparse representation of polynomial. """

    data: dict[MatrixIndex, dict[PolyIndex, Number]]

    def __init__(self, shape: Shape):
        self.data = {}

    def at(self, entry: MatrixIndex, term: PolyIndex) -> Number:
        """ Access polynomial entry. """
        if entry in self.data.keys():
            if term in self.data[entry].keys():
                return self.data[entry][term]
        
        return 0

    def set(self, entry: MatrixIndex, term: PolyIndex, value: Number) -> None:
        """ Set value of polynomial entry. """
        if entry not in self.data.keys():
            self.data[entry] = {}

        self.data[entry][term] = value

    def set_zero(self, entry: MatrixIndex, term: PolyIndex):
        """ Set an entry to zero. """
        del self.data[entry][term]
        if not self.data[entry]:
            del self.data[entry]

    def entries(self) -> Iterable[MatrixIndex]:
        """ Return indices to non-zero entries of the matrix. """
        yield from self.data.keys()

    def terms(self, entry: MatrixIndex) -> Iterable[PolyIndex]:
        """ Return indices to terms with a non-zero coefficient in the
        polynomial at the given matrix entry. """
        if entry in self.data.keys():
            yield from self.data[entry].keys()

        return tuple()


class ScipySparseMatrixRepr(Repr):
    """ Sparse matrix representation of a polynomial.

    Collect the monomials in a vector :math:`x` (the "basis") and store a
    matrix :math:`Q` and such that the quadratic form :math:`x^T Q x` gives the
    polynomial.
    """
    def basis(self):
        raise NotImplementedError

    def matrix(self):
        raise NotImplementedError


class NumpyDenseRepr(Repr):
    """ Dense representation of a polynomial that uses Numpy arrays. """
    data: npt.NDArray

    def __init__(self, shape: Shape):
        self.data = np.zeros(shape)


class NumpyDenseMatrixRepr(Repr):
    """ Dense matrix representation of a polynomials """
