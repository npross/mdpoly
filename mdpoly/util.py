from itertools import tee, filterfalse
from typing import TYPE_CHECKING

from typing import Iterable
from .constants import NUMERICS_EPS

if TYPE_CHECKING:
    pass


def partition(pred, iterable):
    """Partition entries into false entries and true entries.

    If *pred* is slow, consider wrapping it with functools.lru_cache().
    """
    # partition(is_odd, range(10)) --> 0 2 4 6 8   and  1 3 5 7 9
    t1, t2 = tee(iterable)
    return filterfalse(pred, t1), filter(pred, t2)


def isclose(x: float, y: float, eps: float =NUMERICS_EPS) -> bool:
    r""" Check if two numbers are close.

    Close means :math:`|x - y| < \varepsilon` where :math:`\varepsilon` is the
    given ``eps`` or the default value at
    :py:data:`mdpoly.constants.NUMERICS_EPS`.
    """
    # TODO: probably faster with math.isclose
    return abs(x - y) < eps


def iszero(x: float, eps: float =NUMERICS_EPS) -> bool:
    r""" Check a number is zero.

    Being zero means :math:`|x| < \varepsilon` where :math:`\varepsilon` is the given
    ``eps`` or the default value at :py:data:`mdpoly.constants.NUMERICS_EPS`.
    """
    return isclose(x, 0.)


def filtertype(t: type, seq: Iterable):
    """ Filter based on type. """
    yield from filter(lambda x: isinstance(x, t), seq)


def showtree(expr):
    """ Pretty print expression tree. """
    from PrettyPrint import PrettyPrintTree
    def getval(x):
        if x.is_leaf:
            return str(x)
        return x.__class__.__qualname__

    PrettyPrintTree(lambda x: x.children(), getval)(expr)


