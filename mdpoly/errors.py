class AlgebraicError(Exception):
    """ This is raised whenever an invalid mathematical operation is performed.
    See also :py:mod:`mdpoly.operations`.
    """

class InvalidShape(Exception):
    """ This is raised whenever an operation cannot be perfomed because the
    shapes of the inputs do not match. """


class MissingParameters(Exception):
    """ This is raised whenever a parameter was not given. """
