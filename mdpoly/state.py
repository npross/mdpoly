from __future__ import annotations
from typing import TYPE_CHECKING

from .abc import Expr, Var, Param
from .index import PolyVarIndex
from .expressions import Expression
from .errors import MissingParameters

if TYPE_CHECKING:
    from .index import Number
    from .expressions import Expression


Index = int


class State:
    def __init__(self, variables: dict[Var | Expression, Index] = {},
                 parameters: dict[Param | Expression, Number] = {}):

        self._variables: dict[Var, Index] = dict()
        self._parameters: dict[Param, Number] = dict()
        self._last_index: Index = -1 

        for var, idx in variables.items():
            v = var.unwrap() if isinstance(var, Expression) else var

            if not isinstance(v, Var):
                raise IndexError(f"Cannot index {v} (type {type(v)}). "
                        f"Only variables (type {Var}) can be indexed.")

                self._variables[v] = idx
                self._last_index = max(self._last_index, idx)

        # FIXME: allow matrix constants
        for param, val in parameters.items():
            p = param.unwrap() if isinstance(param, Expression) else param

            if not isinstance(p, Param):
                raise IndexError(f"Cannot get parameter {p} (type {type(p)}). "
                        f"Because its type is not {Param}.")

            self._parameters[p] = val


    def _make_index(self) -> Index:
        self._last_index += 1
        return self._last_index

    def index(self, var: Var | Expression) -> Index:
        """ Get the index for a variable. """
        if isinstance(var, Expression):
            var: Expr | Var = var.unwrap() # type: ignore[no-redef]

        if not isinstance(var, Var):
            raise IndexError(f"Cannot index {var} (type {type(var)}). "
                    f"Only variables (type {Var}) can be indexed.")
        
        if var not in self._variables.keys():
            new_index = self._make_index()
            self._variables[var] = new_index
            return new_index

        return self._variables[var]

    def from_index(self, index: Index | PolyVarIndex) -> Var | Number:
        """ Get a variable object from the index.
        This is a reverse lookup operation and it is **not**
        :math:`\mathcal{O}(1)`!"""
        if isinstance(index, PolyVarIndex):
            index = index.var_idx

        if index == -1:
            return 1

        for var, idx in self._variables.items():
            if idx == index:
                return var

        raise IndexError(f"There is no variable with index {index}.")

    def parameter(self, param: Param | Expression) -> Number:
        """ Get the value for a parameter. """
        if isinstance(param, Expression):
            param: Param | Expr = param.unwrap() # type: ignore[no-redef]

        if not isinstance(param, Param):
            raise IndexError(f"Cannot get parameter {param} (type {type(param)}). "
                    f"Because its type is not {Param}.")

        if param not in self._parameters:
            raise MissingParameters("Cannot construct representation because "
                                    f"value for parameter {param} was not given.")

        return self._parameters[param]
