from unittest import TestCase

from .. import Variable, State
from ..index import PolyVarIndex, PolyIndex

class TestPolyVarIndex(TestCase):

    def test_constant(self):
        const = PolyVarIndex.constant()
        self.assertTrue(const.is_constant())

    def test_from_var(self):
        x = Variable("x")
        state = State()
        ix = state.index(x)

        idx = PolyVarIndex.from_var(x, state)
        self.assertTrue(idx.var_idx == ix)

    def test_total_order(self):
        ...


class TestPolyIndex(TestCase):

    def test_magic_methods(self):
        ...

    def test_from_dict(self):
        ...

    def test_multi_index(self):
        ...

    def test_constant(self):
        const = PolyIndex.constant()
        self.assertTrue(const.is_constant())

