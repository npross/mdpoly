from ..abc import Repr
from ..index import Shape, MatrixIndex, PolyIndex, Number


class TestRepr(Repr):
    """ Representation used for test cases """

    def __init__(self, shape: Shape):
        ...

    def at(self, entry: MatrixIndex, term: PolyIndex) -> Number:
        raise NotImplementedError
