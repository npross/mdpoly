""" Tests for :py:mod:`mdpoly.expressions` """

from unittest import TestCase

from .representations import TestRepr

from .. import Variable, Constant, Parameter, State

from ..abc import Expr
from ..errors import AlgebraicError
from ..expressions import PolyConst, Expression
from ..operations import UnaryOp, Reducible
from ..index import Shape, MatrixIndex
from ..representations import SparseRepr


class TestPolyExpressions(TestCase):

    def test_scalar_valid(self):
        x, y = Variable.from_names("x, y")
        k = Constant(2, "k")
        p = Parameter("p")

        ops = (
            x + y, x + k, x + p, k + x, p + x, # addition
            x - y, x - k, x - p, k - x, p - x, # subtraction
            x * y, x * k, x * p, k * x, p * x, # multiplication
            x / k, x / p, # division
            x ** k, x ** p, # exponentiation
            -x, # negation
        )

        for result in ops:
            self.assertIsInstance(result, Expr)

    def test_scalar_invalid(self):
        x, y = Variable.from_names("x, y")
        k = Constant(2, "k")

        self.assertRaises(AlgebraicError, lambda: x / y)
        self.assertRaises(AlgebraicError, lambda: k / x)
        self.assertRaises(AlgebraicError, lambda: x ** y)

    def test_derivative_integral(self):
        x, y, z = Variable.from_names("x, y, z")

        dp_auto = (x ** 2 * y + 2 * x * z + y * z * (x + 1) + 3).diff(x)
        dp_hand = 2 * x * y + 2 * z + y * z # diff wrt x by hand

        # FIXME: use TestRepr
        auto_repr, auto_state = dp_auto.to_repr(SparseRepr, State())
        hand_repr, hand_state = dp_hand.to_repr(SparseRepr, State())

        # FIXME: do not use internal stuff in test, implement TestRepr
        self.assertTrue(auto_repr.data == hand_repr.data)


    def test_wraps_literals(self):
        x = Variable("x")

        ops_right = (x + 2, x * 2, x / 2, x ** 2,)
        ops_left = (2 + x, 2 * x,)

        for result in ops_right:
            self.assertIsInstance(result.right, PolyConst)

        for result in ops_left:
            self.assertIsInstance(result.left, PolyConst)


# ┏━╸╻ ╻╺┳╸┏━╸┏┓╻┏━┓╻┏━┓┏┓╻┏━┓
# ┣╸ ┏╋┛ ┃ ┣╸ ┃┗┫┗━┓┃┃ ┃┃┗┫┗━┓
# ┗━╸╹ ╹ ╹ ┗━╸╹ ╹┗━┛╹┗━┛╹ ╹┗━┛

class Grok(UnaryOp, Reducible):

    @property
    def shape(self) -> Shape:
        return self.left.shape

    def reduce(self) -> Expr:
        return self.left


class WithGrokOp(Expression):

    @Expression.wrap_result
    def grok(self) -> Expr:
        with self as inner:
            return Grok(inner)


class TestExtension(TestCase):

    def test_grok_extension(self):
        x = Variable.from_extension("x", WithGrokOp)
        (x ** 2).grok()

