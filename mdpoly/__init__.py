r""" A Library to represent multivariate polynomials

Overview
========

TODO


Terminology
===========

Herein we will describe the terminology used to talk about polynomials, both in
their mathematical sense as well as their representation in code. Most terms
should already be well known to the reader, and the purpose ir rather to remove
any ambiguities.


Variable, Monomial, Term, Entry, and Coefficient
------------------------------------------------

From a mathematical perspective this library describes objects from
:math:`\mathbb{R}^{n\times m} [x_1, x_2, \ldots, x_k]`, where
:math:`\mathbb{R}^{n\times m}` is the set of matrices with real entries and
:math:`x_i` are called *variables*. The simplest concrete example is
:math:`\mathbb{R}[x]`, the set of scalar univariate polynomials for example

.. math::
    p(x) = x^2 + 2x + 3 \in \mathbb{R}[x].

A scalar multivariate polynomial could be for example:

.. math::
    p(x, y) = x^2 +  2 xy^4 + y + 5 \in \mathbb{R}[x, y].

A *monomial* is a product powers of variables, in the example above
:math:`xy^4` or :math:`x^2`. Note that :math:`1` is also a monomial, the
constant monomial, since it is a product of variables to the zeroth power. In
front of a monomial there is a scalar *coefficient* and we call *term* a
monomial with its coefficient. In code we index a polynomial term using
:py:class:`mdpoly.index.PolyIndex`.

To provide an example of the most general case consider :math:`P(x, y) \in
\mathbb{R}^{2 \times 3} [x, y]`, a matrix of polynomials:

.. math::
    P(x, y) = \begin{bmatrix}
        3 x^2 + y & y + 1 & 0 \\
        y^5 + 8 xy & x + y & 2 y^3 \\
    \end{bmatrix}
    \in \mathbb{R}^{2 \times 3}[x, y].

Since we are dealing with matrices, we say that at the *entry* with row 1 and
column 1 there is a polymial `x^2 + y`. It is important to note that in
mathematics it is customary to start indices at 1, whereas in software
programming at 0. We will index rows and columns starting at 0 in
:py:class:`mdpoly.index.MatrixIndex`.


Expression
----------

Within the code, we will often call something like :math:`x^2 + 1` and
*expression* (for example :py:class:`mdpoly.abc.Expr` or
:py:class:`mdpoly.expressions.poly.PolyExpr`) instead of polynomial.
Expressions are more general and can include any mathematical operation. Of
course in the end and expression should result in a polynomial.


Decision Variables, Program
---------------------------

This package also contains a module :py:mod:`mdpoly.sos` for sum-of-square
polynomials, which involves mathematical programming (optimization). Now,
because of this fact we have to resolve the following ambiguity: consider the
polynomial

.. math::
    p = a^2 x + b x^2 + y \in \mathbb{R}[x, y, a, b].

TODO: difference between variables and decision / optimization variables.


Parameters
----------

From the field of parametric programming we borrow the term *parameter* to mean
a constant that is specified at a later time. Think of physical / design
constants that are subject to change (or are computed by another process) and
should not be hard-coded, but rather only given names.

However, unlike in the discipline parametric programming we may not leave the
parameter unspecified before solving the optimization problem (yet). This is
because the theory of parametric semi-definite programming is still very, very
far from developed (compared to say, parametric LPs).


Representation
--------------

TODO: data structure(s) that represent the polynomial


"""

# internal classes imported with underscore because 
# they should not be exposed to the end users

from .index import (Shape as _Shape)

from .expressions import (Expression as _Expression)
from .leaves import (PolyConst as _PolyConst, PolyVar as _PolyVar, PolyParam as _PolyParam,
                     MatConst as _MatConst, MatVar as _MatVar, MatParam as _MatParam)

from .state import State as _State

from typing import Self, Iterable, Type, TypeVar
from dataclasses import dataclass


# ┏━╸╻ ╻┏━┓┏━┓┏━┓╺┳╸   ┏━┓┏━┓   ╻┏━┓
# ┣╸ ┏╋┛┣━┛┃ ┃┣┳┛ ┃    ┣━┫┗━┓╺━╸┃┗━┓
# ┗━╸╹ ╹╹  ┗━┛╹┗╸ ╹    ╹ ╹┗━┛   ╹┗━┛

Shape = _Shape
State = _State


# ╻ ╻┏━┓┏━┓┏━┓┏━┓┏━╸┏━┓┏━┓
# ┃╻┃┣┳┛┣━┫┣━┛┣━┛┣╸ ┣┳┛┗━┓
# ┗┻┛╹┗╸╹ ╹╹  ╹  ┗━╸╹┗╸┗━┛

WithExtT = TypeVar("WithExtT", bound=_Expression)

# FIXME: move out of this file
class FromHelpers:
    @classmethod
    def from_names(cls, comma_separated_names: str, strip: bool =True) -> Iterable[Self]:
        """ Generate scalar variables from comma separated list of names """
        names: Iterable = comma_separated_names.split(",")
        if strip:
            names = map(str.strip, names)
        yield from map(cls, names)

    @classmethod
    def from_extension(cls, name, ext: Type[WithExtT]) -> WithExtT:
        return ext(expr=cls(name).expr)


@dataclass
class Constant(_Expression, FromHelpers):
    """ Constant values """
    def __init__(self, *args, expr=None, **kwargs):
        # FIXME: make less ugly
        if not expr:
            _Expression.__init__(self, expr=_PolyConst(*args, **kwargs))
        else:
            _Expression.__init__(self, expr=expr)


@dataclass
class Variable(_Expression, FromHelpers):
    """ Polynomial Variable """
    def __init__(self, *args, expr=None, **kwargs):
        if not expr:
            _Expression.__init__(self, expr=_PolyVar(*args, **kwargs))
        else:
            _Expression.__init__(self, expr=expr)


@dataclass
class Parameter(_Expression, FromHelpers):
    """ Parameter that can be substituted """
    def __init__(self, *args, expr=None, **kwargs):
        if not expr:
            _Expression.__init__(self, expr=_PolyParam(*args, **kwargs))
        else:
            _Expression.__init__(self, expr=expr)

    def __hash__(self):
        return hash((self.__class__.__qualname__, hash(self.unwrap())))


class MatrixConstant(_Expression):
    """ Matrix constant """
    

class MatrixVariable(_Expression):
    """ Matrix Polynomial Variable """


class MatrixParameter(_Expression):
    """ Matrix Parameter """


