from __future__ import annotations
from typing import TYPE_CHECKING

from dataclasses import dataclass
from functools import wraps
from typing import Type, Callable, Any, Self, Sequence

from .abc import Expr, Var, Repr
from .index import Number
from .leaves import PolyConst
from .errors import AlgebraicError
from .representations import SparseRepr

from .operations import WithTraceback
from .operations.add import MatAdd, MatSub
from .operations.mul import MatElemMul, MatMul
from .operations.exp import PolyExp
from .operations.derivative import PolyPartialDiff

if TYPE_CHECKING:
    from .abc import ReprT
    from .state import State


# ┏━┓┏━┓┏━╸┏━┓┏━┓╺┳╸╻┏━┓┏┓╻┏━┓
# ┃ ┃┣━┛┣╸ ┣┳┛┣━┫ ┃ ┃┃ ┃┃┗┫┗━┓
# ┗━┛╹  ┗━╸╹┗╸╹ ╹ ╹ ╹┗━┛╹ ╹┗━┛


@dataclass
class Expression:
    """ Wrapper around :py:class:`mdpoly.abc.Expr` that adds operator
    overloading and operations to expression objects. """
    expr: Expr

    # -- Magic methods ---

    def __enter__(self) -> Expr:
        """ Allow nicer notation using the `with` statement.

        ... code:: py
            f(x.expr) # replace this or
            f(x.unwrap()) # this with
            
            with x as expr:
                f(expr)
                ... # do more stuff with expr
        """
        # TODO: not very idiomatic, how much is this unidiomatic?
        return self.unwrap()

    def __exit__(self, exc_type: type, exc_val: Exception, tb):
        """ Context manager exit.

        See :py:meth:`mdpoly.expresssions.Expression.__enter__`.
        """
        return False # Propagate exceptions

    def __str__(self):
        return f"Expression{str(self.expr)}"

    # TODO: is this actually a good idea? probably not.
    # def __getattr__(self, attr):
    #     # Behave transparently
    #     # TODO: replace with selected attributes to expose?
    #     return getattr(self.unwrap(), attr)

    # -- Monadic operations -- 

    def unwrap(self) -> Expr:
        """ Un-wrap value, get the raw expression without operations. """
        return self.expr

    @staticmethod
    def map(fn: Callable[[Expr], Expr]) -> Callable[[Expression], Expression]:
        """ Map in the functional programming sense.

        Converts a function like `(Expr -> Expr)` into
        `(Expression<Expr> -> Expression<Expr>)`.
        """
        @wraps(fn)
        def wrapper(e: Expression) -> Expression:
            return Expression(expr=fn(e.expr))
        return wrapper

    @staticmethod
    def zip(fn: Callable[..., Expr]) -> Callable[..., Expression]:
        """ Zip, in the functional programming sense. 

        Converts a function like `(Expr -> Expr -> ... -> Expr)` into
        `(Expression<Expr> -> Expression<Expr> -> ... -> Expression<Expr>)`.
        """
        @wraps(fn)
        def wrapper(*args: Expression) -> Expression:
            return Expression(expr=fn(*(arg.expr for arg in args)))
        return wrapper

    @staticmethod
    def bind(fn: Callable[[Expr], Expression]) -> Callable[[Expression], Expression]:
        """ Bind in the functional programming sense. Also known as flatmap.

        Converts a funciton like `(Expr -> Expression)` into `(Expression -> Expression)` 
        so that it can be composed with other functions.
        """
        @wraps(fn)
        def wrapper(arg: Expression) -> Expression:
            return fn(arg.expr)
        return wrapper

    @staticmethod
    def wrap_result(meth: Callable[[Expression, Any], Expr]) -> Callable[[Expression, Any], Expression]:
        """ Take a method and wrap its result type.

        Turns method `(Expression, Any) -> Expr)` into 
        `(Expression, Any) -> Expression)`. method arguments are left unchanged.
        This is only for conveniente to avoid always having to wrap the result
        by hand.
        """
        @wraps(meth)
        def meth_wrapper(self, *args, **kwargs) -> Expression:
            # Why type(self)? Because if we are wrapping a subtype of Expression,
            # eg. from an extension we want to preserve its type. See for
            # example mdpoly.test.TestExtensions.
            return type(self)(expr=meth(self, *args, **kwargs))
        return meth_wrapper

    # -- Operator overloading ---

    @classmethod
    def _ensure_is_withops(cls, obj: Expression | Expr | Number) -> Expression:
        """ Ensures that the given object is wrapped with type Expression. """
        if isinstance(obj, Expression):
            return obj

        if isinstance(obj, Expr):
            return Expression(expr=obj)

        # mypy typechecker bug: https://github.com/python/mypy/issues/11673
        elif isinstance(obj, Number): # type: ignore[misc, arg-type]
            return Expression(expr=PolyConst(value=obj)) # type: ignore[call-arg]

        raise TypeError(f"Cannot wrap {obj}!") # FIXME: Decent message

    @wrap_result
    def __add__(self, other: Expression | Number) -> Expr:
        other = self._ensure_is_withops(other)
        with self as left, other as right:
            return MatAdd(left, right)

    @wrap_result
    def __radd__(self, num: Number) -> Expr:
        other = self._ensure_is_withops(num)
        with other as left, self as right:
            return MatAdd(left, right)

    @wrap_result
    def __sub__(self, other: Expression | Number) -> Expr:
        other = self._ensure_is_withops(other)
        with self as left, other as right:
            return MatSub(left, right)

    @wrap_result
    def __rsub__(self, num: Number) -> Expr:
        other = self._ensure_is_withops(num)
        with other as left, self as right:
            return MatSub(left, right)

    def __neg__(self) -> Self:
        raise NotImplementedError

    @wrap_result
    def __mul__(self, other: Expression | Number) -> Expr:
        other = self._ensure_is_withops(other)
        with self as left, other as right:
            return MatElemMul(left, right)

    @wrap_result
    def __rmul__(self, num: Number) -> Expr:
        other = self._ensure_is_withops(num)
        with other as left, self as right:
            return MatElemMul(left, right)

    @wrap_result
    def __pow__(self, num: Number) -> Expr:
        other = self._ensure_is_withops(num)
        with self as left, other as right:
            # TODO: what if it is a matrix? Elementwise power?
            return PolyExp(left, right)

    @wrap_result
    def __matmul__(self, other: Expression) -> Expr:
        other = self._ensure_is_withops(other)
        with self as left, other as right:
            return MatMul(left, right)

    @wrap_result
    def __rmatmul__(self, other: Number | Sequence[Sequence[Number]]) -> Expr:
        # FIXME: _ensure_is_withops does not handle matrices,
        # must fix to take eg row major Sequence[Sequence[Number]] and / or np.NDArray
        other = self._ensure_is_withops(other)
        with other as left, self as right:
            return MatMul(left, right)

    def __truediv__(self, other: Any) -> Self:
        raise NotImplementedError

    def __rtruediv__(self, other: Any) -> Self:
        raise NotImplementedError

    # --- More operations ---

    @wrap_result
    def diff(self, with_respect_to: Expression) -> Expr:
        with self as expr, with_respect_to as wrt:
            if not isinstance(wrt, Var):
                raise AlgebraicError(
                        f"Cannot differentate {str(self.expr)} with respect to "
                        f"{str(wrt)}. We can only differentiate with respect to "
                        f"variables (type {Var}).")

            # TODO: implement vector derivatives
            return PolyPartialDiff(expr, wrt=wrt)

    @wrap_result
    def integrate(self, with_respect_to: Expression) -> Expr:
        raise NotImplementedError

    # -- Make representations ---

    def to_repr(self, repr_type: Type[ReprT], state: State) -> tuple[ReprT, State]:
        """ Construct a representation. """
        # Code below is to improve error messages when an exception occurrs
        # during lazy evaluation.

        def raises(expr: Expr):
            """ Check if an expression raises an exception. """
            try:
                _ = expr.to_repr(repr_type, state)
                return False
            except Exception:
                return True

        def find_bad(expr):
            """ binary search which expression is bad """
            left, right = raises(expr.left), raises(expr.right)
            if not left and not right:
                # left and right are good, exception occurred exactly here
                return expr

            elif left and not right:
                # exception is caused by left
                return find_bad(expr.left)

            elif right and not left:
                # exception is caused by right
                return find_bad(expr.right)

            # Multiple invalid expressions! 
            # We pick one, the other will appear next time.
            return find_bad(expr.left)

        try:
            # try to evaluate exception
            rep, state = self.unwrap().to_repr(repr_type, state)
            return rep, state

        except Exception as e:
            bad = find_bad(self.unwrap())
            # for frame in bad._stack:
            #     print(bad, frame.function, frame.filename, frame.lineno)

            import traceback
            stackstr = "".join(traceback.format_stack(bad._stack[0].frame))
            raise RuntimeError(f"Expression {str(bad)} caused the previous exception. "
                               f"Specifically: \n {repr(bad)}. \n\n"
                               "Call stack at the time when the bad expression "
                               "object was constructed: \n" + stackstr) from e

        finally:
            def clear_stack(expr: Expr):
                """ Recursively clear stack objects to free references. """
                if isinstance(expr, WithTraceback):
                    expr._stack.clear()

                if not expr.is_leaf:
                    clear_stack(expr.left)
                    clear_stack(expr.right)

            clear_stack(self.unwrap())

    def to_sparse(self, state: State) -> tuple[SparseRepr, State]:
        """ Make a sparse representation.

        See :py:class:`mdpoly.representations.SparseRepr`
        """
        return self.to_repr(SparseRepr, state)

    def to_dense(self, state: State) -> tuple[Repr, State]:
        raise NotImplementedError
