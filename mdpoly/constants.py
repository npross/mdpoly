NUMERICS_EPS = 1e-12
r""" Value of :math:`\varepsilon` used for floating point computations.
    Floating point numbers smaller than :math:`\varepsilon` are considered
    as being equal to zero. """
