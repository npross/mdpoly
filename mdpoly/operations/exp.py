from __future__ import annotations
from typing import TYPE_CHECKING

from operator import mul as opmul
from typing import cast
from functools import reduce
from dataclasses import dataclass

from ..abc import Expr, Const
from ..errors import AlgebraicError
from ..index import Shape

from . import BinaryOp, Reducible
from .mul import PolyMul


# TODO: implement matrix exponential, use caley-hamilton thm magic
@dataclass(eq=False)
class MatExp(BinaryOp, Reducible):
    def __init__(self):
        raise NotImplementedError


@dataclass(eq=False)
class PolyExp(BinaryOp, Reducible):
    """ Exponentiation operator between scalar polynomials. """
    shape: Shape = Shape.scalar()

    @property # type: ignore[override]
    def right(self) -> Const: # type: ignore[override]
        if not isinstance(super().right, Const):
            raise AlgebraicError(f"Cannot raise {str(self.left)} to {str(super().right)} "
                                 f"because {str(super().right)} is not a constant.")

        return cast(Const, super().right)

    @right.setter
    def right(self, right: Expr) -> None:
        # Workaround necessary because of a bug
        # https://bugs.python.org/issue14965
        BinaryOp.right.fset(self, right) # type: ignore

    def reduce(self) -> Expr:
        """ See :py:meth:`mdpoly.expressions.Reducible.reduce`. """
        var = self.left
        if not isinstance(self.right.value, int):
            raise NotImplementedError("Cannot raise to non-integer powers (yet).")

        if self.right.value == 0:
            # FIXME: return constant
            raise NotImplementedError

        elif self.right.value < 0:
            raise AlgebraicError("Cannot raise to negative powers (yet).")

        ntimes = self.right.value - 1
        return reduce(PolyMul, (var for _ in range(ntimes)), var)

    def __str__(self) -> str:
        return f"({self.left} ** {self.right})"
