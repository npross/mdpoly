from __future__ import annotations
from typing import TYPE_CHECKING

from dataclasses import dataclass

from . import UnaryOp

if TYPE_CHECKING:
    from ..index import Shape


@dataclass(eq=False)
class MatTranspose(UnaryOp):
    """ Matrix transposition """

    @property
    def shape(self) -> Shape:
        """ See :py:meth:`mdpoly.abc.Expr.shape`. """
        return Shape(self.inner.shape.cols, self.inner.shape.rows)
