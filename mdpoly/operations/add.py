from __future__ import annotations
from typing import TYPE_CHECKING

from typing import Type
from dataclassabc import dataclassabc

from ..abc import Expr
from ..leaves import PolyConst
from ..errors import AlgebraicError

from . import BinaryOp, Reducible
from .mul import MatScalarMul

if TYPE_CHECKING:
    from ..abc import ReprT
    from ..index import Shape
    from ..state import State


@dataclassabc(eq=False)
class MatAdd(BinaryOp): 
    """ Addition operator between matrices. """

    @property
    def shape(self) -> Shape:
        """ See :py:meth:`mdpoly.abc.Expr.shape`. """
        if not self.left.shape == self.right.shape:
            raise AlgebraicError(f"Cannot add matrices {self.left} and {self.right} "
                                 "with different shapes.")
        return self.left.shape


    def to_repr(self, repr_type: Type[ReprT], state: State) -> tuple[ReprT, State]:
        """ See :py:meth:`mdpoly.abc.Expr.to_repr`. """
        # Make a new empty representation
        r = repr_type(self.shape)

        # Make representations for existing stuff
        for node in self.children():
            nrepr, state = node.to_repr(repr_type, state)

            # Process non-zero terms
            for entry in nrepr.entries():
                for term in nrepr.terms(entry):
                    s = r.at(entry, term) + nrepr.at(entry, term)
                    r.set(entry, term, s)

        return r, state

    def __str__(self) -> str:
        return f"({self.left} + {self.right})"


@dataclassabc(eq=False)
class MatSub(BinaryOp, Reducible):
    """ Subtraction operator between matrices. """

    @property
    def shape(self) -> Shape:
        """ See :py:meth:`mdpoly.abc.Expr.shape`. """
        if not self.left.shape == self.right.shape:
            raise AlgebraicError(f"Cannot subtract matrices {self.left} and {self.right} "
                                 f"with different shapes, {self.left.shape} and {self.right.shape}.")
        return self.left.shape

    def reduce(self) -> Expr:
        """ See :py:meth:`mdpoly.expressions.Reducible.reduce`. """
        return MatAdd(self.left, MatScalarMul(PolyConst(-1), self.right))

    def __str__(self) -> str:
        return f"({self.left} - {self.right})"
