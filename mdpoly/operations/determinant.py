from __future__ import annotations
from typing import TYPE_CHECKING

from typing import Type
from dataclassabc import dataclassabc

from ..abc import ReprT
from ..index import Shape
from ..errors import AlgebraicError

from . import UnaryOp

if TYPE_CHECKING:
    from ..abc import ReprT
    from ..state import State


@dataclassabc(eq=False)
class Det(UnaryOp): 
    """ Compute the determinant of a matrix. """

    @property
    def shape(self) -> Shape:
        if not self.inner.shape.cols == self.inner.shape.rows:
            raise AlgebraicError("Cannot take determinant of non-square matrix "
                                 f"{str(self)} (shape {self.inner.shape}).")

        return Shape.scalar()

    def to_repr(self, repr_type: Type[ReprT], state: State) -> tuple[ReprT, State]:
        """ See :py:meth:`mdpoly.abc.Expr.to_repr`. """
        # Make a new empty representation
        r = repr_type(self.shape)

        # Compute LU decomposition by Doolittle's method
        lower = repr_type(self.inner.shape)
        upper = repr_type(self.upper.shape)

        for i in range(self.inner.shape.rows):
            raise NotImplementedError

        return r, state

    
    def __str__(self):
        return "(det {self.inner})"
