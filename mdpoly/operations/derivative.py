from __future__ import annotations
from typing import TYPE_CHECKING

from typing import Type, Self, cast
from dataclassabc import dataclassabc

from ..abc import Expr, Var
from ..errors import AlgebraicError
from ..index import Shape, MatrixIndex, PolyIndex

from . import UnaryOp

if TYPE_CHECKING:
    from ..abc import ReprT
    from ..state import State


# TODO: implement vector derivatives


@dataclassabc(eq=False)
class PolyPartialDiff(UnaryOp):
    """ Partial differentiation of scalar polynomials. """
    wrt: Var 

    @property
    def shape(self) -> Shape:
        """ See :py:meth:`mdpoly.abc.Expr.shape`. """
        return self.inner.shape

    def to_repr(self, repr_type: Type[ReprT], state: State) -> tuple[ReprT, State]:
        """ See :py:meth:`mdpoly.abc.Expr.to_repr`. """
        r = repr_type(self.shape)
        lrepr, state = self.inner.to_repr(repr_type, state)

        entry = MatrixIndex.scalar()
        wrt = state.index(self.wrt)

        for term in lrepr.terms(entry):
            # sadly, walrus operator does not support unpacking
            # https://bugs.python.org/issue43143
            # if ((newterm, fac) := PolyIndex.differentiate(term, wrt)):
            if result := PolyIndex.differentiate(term, wrt):
                newterm, fac = result
                r.set(entry, newterm, fac * lrepr.at(entry, term))

        return r, state

    def __str__(self) -> str:
        return f"(∂_{self.wrt} {self.inner})"

    def replace(self, old: Expr, new: Expr) -> Self:
        """ Overloads :py:meth:`mdpoly.abc.Expr.replace` """
        if self.wrt == old:
            if not isinstance(new, Var):
                # FIXME: implement chain rule
                raise AlgebraicError(f"Cannot take a derivative with respect to {new}.")

            self.wrt = cast(Var, new)
        return cast(Self, Expr.replace(self, old, new))
