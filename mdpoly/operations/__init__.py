from __future__ import annotations
from typing import TYPE_CHECKING

from typing import Type
from abc import abstractmethod
from dataclasses import dataclass, field
from dataclassabc import dataclassabc
from inspect import stack

from ..abc import ReprT, Expr, Nothing

if TYPE_CHECKING:
    from ..state import State


@dataclass
class WithTraceback:
    """ Dataclass that contains traceback information. 

    See where it is used in :py:meth:`mdpoly.expressions.Expresion.to_repr`.
    """
    def __post_init__(self):
        # NOTE: leaving this information around indefinitely can cause memory
        # leaks, it must be cleared by calling `self._stack.clear()`.
        self._stack = stack()


@dataclassabc(eq=False)
class BinaryOp(Expr, WithTraceback):
    """ Binary operator.

    TODO: desc
    """
    left: Expr = field()
    right: Expr = field()


@dataclassabc(eq=False)
class UnaryOp(Expr, WithTraceback):
    """ Unary operator.

    TODO: desc
    """
    right: Expr = field()

    @property
    def inner(self) -> Expr:
        """ Inner expression on which the operator is acting, alias for right. """
        return self.right

    @property
    def left(self) -> Expr:
        return Nothing()

    @left.setter
    def left(self, left) -> None:
        if not isinstance(left, Nothing):
            raise ValueError("Cannot set left of left-acting unary operator "
                             "to something that is not of type Nothing.")


class Reducible(Expr):
    """ Reducible Expression.

    Algebraic expression that can be written in terms of other (existing)
    expression objects, i.e. can be reduced to another expression made of
    simpler operations. For example subtraction can be written in term of
    addition and multiplication.
    """

    def to_repr(self, repr_type: Type[ReprT], state: State) -> tuple[ReprT, State]:
        """ See :py:meth:`mdpoly.abc.Expr.to_repr`. """
        return self.reduce().to_repr(repr_type, state)

    @abstractmethod
    def reduce(self) -> Expr:
        """ Reduce to an expression made of other operations that have already
        implemented :py:meth:`mdpoly.abc.Expr.to_repr`. """


# TODO: review this idea before implementing
# class Simplifiable(Expr):
#     """ Simplifiable Expression. """
# 
#     @abstractmethod
#     def simplify(self) -> Expr:
#         """ Simplify the expression """

